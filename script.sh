#!/bin/bash
# Make local backup my system with restic.

# Exit on failure, pipe failure
set -e -o pipefail

# Redirect stdout ( > ) into a named pipe ( >() ) running "tee" to a file, so we can observe the status by simply tailing the log file.
me=$(basename "$0")
now=$(date +%F_%R)
log_dir=/var/log/restic
log_file="${log_dir}/${now}_${me}.$$.log"
test -d $log_dir || mkdir -p $log_dir
exec > >(tee -i $log_file)
exec 2>&1

# Clean up lock if we are killed.
# If killed by systemd, like $(systemctl stop restic), then it kills the whole cgroup and all it's subprocesses.
# However if we kill this script ourselves, we need this trap that kills all subprocesses manually.
exit_hook() {
	echo "In exit_hook(), being killed" >&2
	jobs -p | xargs kill
	/usr/bin/restic unlock
}
trap exit_hook INT TERM


# Get timestamp
timestamp() {
	date +%s
}


# GRAPHITE Variables
# Set Graphite host
GRAPHITE=graphite.w2z.com.br
GRAPHITE_PORT=2003
DATE=`date +%s`


# Set all environment variables
source /root/.restic-env


# Status 2 - Its running!
RUNCHECK=2
echo "restic.${SVR_NAME}.db.error ${RUNCHECK} ${DATE}" | nc -w10 $GRAPHITE $GRAPHITE_PORT &
wait $!


# DB Export
USER="root"
OUTPUT="/root/sqldump/"
rm $OUTPUT*.sql > /dev/null 2>&1
databases=`mysql --user=$USER -e "SHOW DATABASES;" | tr -d "| " | grep -v Database`
for db in $databases; do
	if [[ "$db" != "information_schema" ]] && [[ "$db" != "performance_schema" ]] && [[ "$db" != _* ]] ; then
		echo "Dumping database: $db"
		mysqldump --force --opt --user=$USER --databases $db > $OUTPUT$db.sql
	fi
done &
wait $!


# Verifica o SQLDUMP - 1:success 0:error
if [ "$?" -ne 0 ]; then
	RUNCHECK=0
else
	RUNCHECK=1
fi
echo "restic.${SVR_NAME}.db.error ${RUNCHECK} ${DATE}" | nc -w10 $GRAPHITE $GRAPHITE_PORT &
wait $!


# RESTIC Variables
BACKUP_INCLUDE="--files-from /root/.restic-include"
BACKUP_EXCLUDE="--exclude-file /root/.restic-exclude"


# Remove locks from other stale processes to keep the automated backup running.
/usr/bin/restic unlock &
wait $!


# Envia o timestamp inicial para o Graphite
echo "restic.${SVR_NAME}.time.start $(timestamp) ${DATE}" | nc -w10 $GRAPHITE $GRAPHITE_PORT &
wait $!


# Status 2 - Its running!
RUNCHECK=2
echo "restic.${SVR_NAME}.data.error ${RUNCHECK} ${DATE}" | nc -w10 $GRAPHITE $GRAPHITE_PORT &
wait $!


# BACKUP
/usr/bin/restic backup --verbose $BACKUP_INCLUDE $BACKUP_EXCLUDE &
wait $!


# Verifica a resposta do restic BACKUP - 1:success 0:error
if [ "$?" -ne 0 ]; then
	RUNCHECK=0
else
	RUNCHECK=1
fi
echo "restic.${SVR_NAME}.data.error ${RUNCHECK} ${DATE}" | nc -w10 $GRAPHITE $GRAPHITE_PORT &
wait $!


# Status 3 - Its forgetting!
RUNCHECK=3
echo "restic.${SVR_NAME}.data.error ${RUNCHECK} ${DATE}" | nc -w10 $GRAPHITE $GRAPHITE_PORT &
wait $!


# See restic-forget(1) or http://restic.readthedocs.io/en/latest/060_forget.html
/usr/bin/restic forget --verbose --keep-daily 7 --keep-weekly 5 --keep-monthly 6 --prune &
wait $!


# Verifica a resposta do restic FORGET+PRUNE - 1:success 0:error 
if [ "$?" -ne 0 ]; then
	RUNCHECK=0
else
	RUNCHECK=1
fi
echo "restic.${SVR_NAME}.data.error ${RUNCHECK} ${DATE}" | nc -w10 $GRAPHITE $GRAPHITE_PORT &
wait $!


# Envia o timestamp final para o Graphite
echo "restic.${SVR_NAME}.time.end $(timestamp) ${DATE}" | nc -w10 $GRAPHITE $GRAPHITE_PORT &
wait $!


# Clear cache
/usr/bin/restic cache --max-age 1 --cleanup
