#!/bin/bash
# Setup restic.

# Exit on failure, pipe failure
set -e -o pipefail


# Criando diretórios
mkdir /var/log/restic
mkdir /root/sqldump


# Criando arquivos
echo '::Criando variáveis de ambiente...'
touch /root/.restic-env

read -p "Server name: " inputServer
echo export SVR_NAME=$inputServer >> /root/.restic-env
echo export RESTIC_REPOSITORY=b2:vps-2019:$inputServer >> /root/.restic-env

read -p "Mysql password: " inputMysql
echo export MYSQL_PWD=\'$inputMysql\' >> /root/.restic-env

read -p "BackBlaze ID: " inputB2id
echo export B2_ACCOUNT_ID=$inputB2id >> /root/.restic-env

read -p "BackBlaze Key: " inputB2key
echo export B2_ACCOUNT_KEY=$inputB2key >> /root/.restic-env

read -p "Restic Repo Password: " inputResticPass
echo export RESTIC_PASSWORD=\'$inputResticPass\' >> /root/.restic-env

echo '::Variáveis criadas:'
cat /root/.restic-env

echo '::Organizando arquivos de configuração...'
touch /root/sqldump/test.sql
mv .include /root/.restic-include
mv .exclude /root/.restic-exclude
mv script.sh /root/restic.sh


# Permissões
chmod +x /root/restic.sh
chmod 775 /var/log/restic
chmod 775 /root/sqldump
ls -la /root/


# Iniciando REPO
echo '::Iniciando repositório do Restic...'
source /root/.restic-env
/usr/bin/restic init &
wait $!


# Add Crontab
echo '::Adicionando crontab 0 4 * * *...'
crontab -l > resticcron
echo "0 4 * * * /root/restic.sh >/dev/null 2>&1" >> resticcron
crontab resticcron
rm resticcron
crontab -l | grep 'restic'

rm -rf -- "$(pwd -P)" && cd ..
echo '::Script finalizado.'
