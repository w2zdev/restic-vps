# Instruções

## 1. Instale o Restic no servidor:
`https://restic.readthedocs.io/en/latest/020_installation.html`

Ubuntu 16.04:
Instale a ultima versão https://github.com/restic/restic/releases
```
# wget https://github.com/restic/restic/releases/download/v0.9.6/restic_0.9.6_linux_386.bz2
# bzip2 -d restic_0.8.3_linux_386.bz2
# chmod +x restic_0.8.3_linux_386
# mv restic_0.8.3_linux_386 /usr/bin/restic
```

## 2. Clone o projeto na VPS:
`git clone https://rafaelwhs@bitbucket.org/w2zdev/restic-vps.git`

## 3. Execute o arquivo `setup.sh`
## 4. Edite o arquivo `~/.restic-include` e `~/.restic-exclude`